#!/bin/bash

echo -e "\nIniciando containers do GeoServer\n"

sudo docker rm $(sudo docker ps -qa --no-trunc --filter "status=exited")
sudo docker rm $(sudo docker ps -qa --no-trunc --filter "status=created")

sudo docker run -it --rm quay.io/geodocker/base:latest java -version
sudo docker run --name geoserver -e AUTHOR="Tharles" -v $HOME/Geoprocessamento/geoserver/data:/opt/tomcat/webapps/geoserver/data -d -p 9090:9090 quay.io/geodocker/geoserver

echo -e "\n"
