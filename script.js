var map = new L.Map('map', {
    center: new L.LatLng(-16.6846,-49.2634),
    zoom: 6,
    attributionControl:true,
    zoomControl:true
});

var osmUrl='http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
var osmAttrib='GeoMapas de Goiás <a href="http://www.mpgo.mp.br/portal/principal"> MPGO</a>';

var osm = new L.TileLayer(osmUrl, {
    attribution: osmAttrib
});

map.addLayer(osm);

var wmsLayerPetrografias= L.tileLayer.betterWms("http://localhost:9090/geoserver/geomesa/wms/", {
    layers: 'geomesa:Petrografias',
    format: 'image/png',
    transparent: true
});

var wmsLayerGeologias= L.tileLayer.betterWms("http://localhost:9090/geoserver/geomesa/wms/", {
    layers: 'geomesa:Geologias',
    format: 'image/png',
    transparent: true,
    opacity: 0.3
});

var wmsLayerRodovias= L.tileLayer.betterWms("http://localhost:9090/geoserver/geomesa/wms/", {
    layers: 'geomesa:Rodovias',
    format: 'image/png',
    transparent: true,
    opacity: 0.9
});


var wmsLayerMunicipios= L.tileLayer.betterWms("http://localhost:9090/geoserver/geomesa/wms/", {
    layers: 'geomesa:Municipios',
    format: 'image/png',
    transparent: true,
    opacity: 0.3
});

var layers = {
    "Municípios": wmsLayerMunicipios,
    "Geologias" : wmsLayerGeologias,
    "Rodovias" : wmsLayerRodovias,
    "Petrografias" : wmsLayerPetrografias
}

L.control.layers(layers).addTo(map);